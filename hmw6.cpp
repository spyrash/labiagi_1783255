#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <move_base_msgs/MoveBaseAction.h>


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main (int argc, char **argv)
{
  ros::init(argc, argv, "hmw6");

  // create the action client
  // true causes the client to spin its own thread
  MoveBaseClient ac("move_base", true);
  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  ac.waitForServer(); //will wait for infinite time

  ROS_INFO("Action server started, sending goal.");
  
  move_base_msgs::MoveBaseGoal goal;
  goal.target_pose.header.frame_id = "base_link";
  goal.target_pose.header.stamp = ros::Time::now();
  //voglio salvarmi la posizione iniziale target_pose.pose.position.x
  //goal.target_pose.pose.position.x = -13;
  //goal.target_pose.pose.orientation.w = 1;
  //goal.target_pose.pose.position.y = 22;
    goal.target_pose.pose.position.x = -14.53;
  goal.target_pose.pose.position.y = 23.24;
  //goal.target_pose.pose.position.z = -0.000308037;
  ROS_INFO("Sending goal");
  ac.sendGoal(goal);
  ac.waitForResult();
  ros::Duration(20.0).sleep();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
	ROS_INFO("Goal completato! Ora, procedo a cancellarli.");
  else
	ROS_INFO("Errore.");
		
  ac.cancelAllGoals();
	
  //wait for the action to return


  goal.target_pose.pose.position.x = -11.27;
  goal.target_pose.pose.position.y = 23.26;
  //goal.target_pose.pose.orientation.z = 1.0;
  ROS_INFO("Sending new goal");
  ac.sendGoal(goal);
  ac.waitForResult();
  
  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
	ROS_INFO("Goal completato");
  else ROS_INFO("Errore.");
  //exit
  return 0;
}
