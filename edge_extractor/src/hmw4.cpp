#include <iostream>
#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h> 
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <stdlib.h>

void callback(const sensor_msgs::CompressedImageConstPtr& msg){
	cv_bridge::CvImagePtr cv_ptr=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
	cv::imshow("show_duckie",cv_ptr->image);
	cv::waitKey(10);
	cv::Mat A,B,C;
	cv::cvtColor(cv_ptr->image,A,CV_BGR2GRAY);
	GaussianBlur(A,B,cv::Size(3,3),0,0,0);
	cv::Canny(B,C,300,3);
	cv::imshow("show_homework",C);
	return;
}
int main(int argc,char** argv){
	ros::init(argc,argv,"Homework");
	ros::NodeHandle s;
	ros::Subscriber ServiceImage = s.subscribe("/default/camera_node/image/compressed",1,callback);
	ros::spin();
	return 1;
}
