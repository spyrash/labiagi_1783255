#include "ros/ros.h"
#include "turtlesim/SpawnCircle.h"
#include "turtlesim/Circle.h"
#include "turtlesim/Spawn.h"
#include "turtlesim/GetCircles.h"
#include <vector>
#include "turtlesim/Kill.h"
#include "turtlesim/Pose.h"
int contatore_id = 2;
std::vector<turtlesim::Circle> VectorCircle;
bool callSpawn(turtlesim::SpawnCircle::Request &req, turtlesim::SpawnCircle::Response &res){
	   turtlesim::Circle cir;
	   cir.x = (float)req.x;
	   cir.y = (float)req.y;
	   cir.id = contatore_id;
	   VectorCircle.push_back(cir);
	   ros::NodeHandle sp;
       ros::ServiceClient client = sp.serviceClient<turtlesim::Spawn>("/spawn");
       turtlesim::Spawn srv;
       srv.request.x = (float)req.x;
       srv.request.y = (float)req.y;
       srv.request.theta = 1.0;
       std::stringstream st;
       st << "turtle" << contatore_id++;
       srv.request.name=st.str();
       res.circles=VectorCircle;
       if (client.call(srv)){
				ROS_INFO("eseguito correttamente spawn");
      }
      else{
      ROS_ERROR("fallita chiamata spawn");
      return 1;
              } 
}

bool callSpawnGet(turtlesim::GetCircles::Request &req, turtlesim::GetCircles::Response &res){
	res.circles=VectorCircle;
	return true;
}
void callSpawnPosition(const turtlesim::Pose & pose_msg){
	int ind=0;
	for(turtlesim::Circle cerchio : VectorCircle){
		if((round(cerchio.x) == round(pose_msg.x)) && (round(cerchio.y) == round(pose_msg.y)) ){
			ros::NodeHandle n;
			ros::ServiceClient client = n.serviceClient<turtlesim::Kill>("/kill");
			turtlesim::Kill srv;
			std::stringstream st;
			st << "turtle" << (int)cerchio.id;
			srv.request.name=st.str();
			if(!client.call(srv)){
				ROS_ERROR(" fallita la chiamata kill");
				return;
			}
			else{
				VectorCircle.erase(VectorCircle.begin()+ind);
				std::cerr << "lunghezza VectorCircle: " << VectorCircle.size() << std::endl;
			}
		}
		ind++;
	}
	return;
}
	
int main (int argc, char** argv){
	ros::init(argc,argv,"serviceCircle");
	ros::NodeHandle s;
	ros::ServiceServer serviceSpawn = s.advertiseService("spawn_circle", callSpawn);
	ros::ServiceServer serviceGet = s.advertiseService("getCircles",callSpawnGet);
	ros::Subscriber servicePosition = s.subscribe("turtle1/pose", 1,callSpawnPosition);
	ROS_INFO("usare 'rosrun turtlesim tutorial:draw_cricle_client N' con N numero di cerchi(e tartarughe )");
	ros::spin();
	return 0;
}
