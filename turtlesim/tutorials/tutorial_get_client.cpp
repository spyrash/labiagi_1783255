#include "ros/ros.h"
#include "turtlesim/SpawnCircle.h"
#include "turtlesim/Circle.h"
#include "turtlesim/Spawn.h"
#include "turtlesim/GetCircles.h"
#include <vector>
#include "turtlesim/Kill.h"
#include "turtlesim/Pose.h"
    #include <cstdlib>
    #include <time.h>
    #include <ctime>
    #include <stdlib.h>
    #include <iostream>
int main(int argc,char **argv){
	ros::init(argc,argv,"tutorial_get_client");
	ros::NodeHandle s;
	ros::ServiceClient client = s.serviceClient<turtlesim::GetCircles>("getCircles");
	turtlesim::GetCircles srv;
	if(client.call(srv)){
		std::vector<turtlesim::Circle> VectorCircle = srv.response.circles;
		std::cout << "[";
		for(turtlesim::Circle i : VectorCircle){
			std::cout << " (" << (int)i.id << ", "<< i.x <<", "<<i.y<<" )";
		}
		std::cout << "]" << std::endl;
	}
	else{
		ROS_ERROR(" fallita chiamata get circles");
		return 1;
	}
	return 0;
}
