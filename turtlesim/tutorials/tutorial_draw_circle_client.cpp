#include "ros/ros.h"
#include "turtlesim/Circle.h"
#include "turtlesim/Spawn.h"
 #include <turtlesim/SpawnCircle.h>
    #include <cstdlib>
    #include <time.h>
    #include <ctime>
    #include <stdlib.h>
    #include <iostream>
       int main(int argc, char **argv){
      ros::init(argc, argv, "tutorial_draw_circle_client");
      if (argc < 2 ){
       ROS_INFO("input sbagliato, metti un numero N (spawnerò N cerchi)");
       return 1;
     }
       ros::NodeHandle n;
 ros::ServiceClient client = n.serviceClient<turtlesim::SpawnCircle>("spawn_circle");
  int numeriCerchi=atoi(argv[1]);
  srand(time(NULL));
 while(numeriCerchi--){
     turtlesim::SpawnCircle srv;
     srv.request.x = rand()%10;
     srv.request.y = rand()%10;
     if (client.call(srv)){
	     ROS_INFO("eseguito correttamente spawncircle");
     }
     else{
     ROS_ERROR("fallita la chiamata SpawnCircle");
      return 1;   
         } 
             }
return 0;
}

